import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getPromotion } from "./promotionSlice";

const Promotion = () => {
  const { promotion } = useSelector((state) => state.promotion);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getPromotion());
  }, []);
  return (
    <div className=" container">
      <p>{promotion}</p>
    </div>
  );
};

export default Promotion;
