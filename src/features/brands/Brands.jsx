import React from "react";
import brand1 from "../../assets/brands/brand1.png";
import brand2 from "../../assets/brands/brand2.png";
import brand3 from "../../assets/brands/brand3.png";
import brand4 from "../../assets/brands/brand4.png";
import brand5 from "../../assets/brands/brand5.png";
const Brands = () => {
  return (
    <div className=" container">
      <div className="mt-[114px]">
        <h1 className="lg:text-4xl">Бренды</h1>
      </div>
      <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto xl:flex-wrap gap-4">
        <div className="mt-[30px] min-w-[140px] h-[170px] lg:w-[160px] lg:h-[160px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill">
          <img src={brand1} alt="" />
        </div>
        <div className="mt-[30px] min-w-[140px] h-[170px] lg:w-[160px] lg:h-[160px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill">
          <img src={brand2} alt="" />
        </div>
        <div className="mt-[30px] min-w-[140px] h-[170px] lg:w-[160px] lg:h-[160px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill">
          <img src={brand3} alt="" />
        </div>
        <div className="mt-[30px] min-w-[140px] h-[170px] lg:w-[160px] lg:h-[160px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill">
          <img src={brand4} alt="" />
        </div>
        <div className="mt-[30px] min-w-[140px] h-[170px] lg:w-[160px] lg:h-[160px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill">
          <img src={brand5} alt="" />
        </div>
      </div>
    </div>
  );
};

export default Brands;
