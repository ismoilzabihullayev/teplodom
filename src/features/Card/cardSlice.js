import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  card: [],
};

const cardSlice = createSlice({
  name: "card",
  initialState,
  reducers: {
    addToBasket: (state, { payload }) => {
      state.card = state.card.filter((product) => product.id !== payload.id);
      state.card = [...state.card, payload];
    },
    deleteToBasket: (state, action) => {
      state.card = state.card.filter((product) => {
        product.id !== action.payload;
      });
    },
  },
});
export const { addToBasket, deleteToBasket } = cardSlice.actions;

export default cardSlice.reducer;

export function getCard() {
  return async function (dispatch) {
    const res = await fetch(`http://localhost:3333/productnew`);
    const data = await res.json();
    dispatch({ type: "card/getCard", payload: data });
  };
}
