import React from "react";
import { useDispatch, useSelector } from "react-redux";
import ProductCard from "../../components/ProductCard";
import oformith from "../../assets/card/oformith.png";
import delet from "../../assets/card/delete.png";
import { deleteToBasket } from "./cardSlice";

const Card = () => {
  const { card } = useSelector((state) => state.card);
  const dispatch = useDispatch();
  return (
    <div className="lg:container mx-auto px-4 mb-20">
      <h2 className=" text-[20px] font-bold mb-[30px] sm:text-[30px]">
        Корзинка
      </h2>
      <div className=" grid grid-cols-1 xl:grid-cols-2 sm:grid-cols-4 gap-4">
        {card?.map((product, i) => {
          return (
            <div>
              <ProductCard key={product.id} product={product} />
              <div className="flex mt-9 ml-1">
                <button className="btn flex items-center gap-3 xl:px-5">
                  <img src={oformith} alt="" className="" />
                  <span className="">Оформить</span>
                </button>
                <button
                  className="bg-[#FFB12A] ml-[83px] rounded-md px-2 py-2"
                  onClick={() => dispatch(deleteToBasket(product))}
                >
                  <img src={delet} alt="" />
                </button>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Card;
