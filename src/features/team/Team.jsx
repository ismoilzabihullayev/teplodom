import React from "react";
import firstperson from "../../assets/team/First-Person.png";
import secondPerson from "../../assets/team/second-person.png";
import threePerson from "../../assets/team/three-person.png";
import fourPerson from "../../assets/team/four-person.png";
const Team = () => {
  return (
    <div className="container ">
      <div>
        <div className="mt-[117px]">
          <h1 className="text-4xl">Наши команда</h1>
        </div>
        <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto xl:flex-wrap gap-4">
          <div className="min-w-[140px] h-[170px] lg:w-[255px] lg:h-[295px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill	">
            <div className="flex flex-1 items-center justify-center">
              <img src={firstperson} className="max-w-[155px] h-[156px]" />
            </div>
            <p className="text-center text-1xl line-clamp-2 mb-[10px] ">
              Ӯткуров Сардор
            </p>
            <p className="text-center line-clamp-2 text-[#878787] text-sm ">
              Директор фирмы
            </p>
          </div>
          <div className="min-w-[140px] h-[170px] lg:w-[255px] lg:h-[295px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill	">
            <div className="flex flex-1 items-center justify-center">
              <img src={secondPerson} className="max-w-[155px] h-[156px]" />
            </div>
            <p className="text-center text-1xl line-clamp-2 mb-[10px] ">
              Рахматуллаев Хаб
            </p>
            <p className="text-center line-clamp-2 text-[#878787] text-sm ">
              Менеджерпо продажам
            </p>
          </div>
          <div className="min-w-[140px] h-[170px] lg:w-[255px] lg:h-[295px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill	">
            <div className="flex flex-1 items-center justify-center">
              <img src={threePerson} className="max-w-[155px] h-[156px]" />
            </div>
            <p className="text-center text-1xl line-clamp-2 mb-[10px] ">
              Усмонов Нодир
            </p>
            <p className="text-center line-clamp-2 text-[#878787] text-sm ">
              Менеджерпо продажам
            </p>
          </div>
          <div className="min-w-[140px] h-[170px] lg:w-[255px] lg:h-[295px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill	">
            <div className="flex flex-1 items-center justify-center">
              <img src={fourPerson} className="max-w-[155px] h-[156px]" />
            </div>
            <p className="text-center text-1xl line-clamp-2 mb-[10px] ">
              Йолдошев Донийор
            </p>
            <p className="text-center line-clamp-2 text-[#878787] text-sm ">
              Менеджерпо продажам
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Team;
