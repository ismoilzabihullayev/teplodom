import React from "react";
import vector from "../../assets/advantages/Vector.svg";
import selfAdhesive from "../../assets/advantages/self-adhesive.svg";
import market from "../../assets/advantages/Market.svg";
import box from "../../assets/advantages/box.svg";
import car from "../../assets/advantages/car.svg";
const Advantages = () => {
  return (
    <div className=" container ">
      <div className="mt-[100px]">
        <div className=" flex mb-8">
          <h1 className=" lg:text-4xl">Наши преимущества</h1>
        </div>
        <div>
          <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto xl:flex-wrap gap-4">
            <div className=" min-w-[72px] h-[72px] flex items-center">
              <div className=" bg-blue-300 w-[72px] h-[72px] rounded-full">
                <img src={vector} alt="" className=" ml-[10px] mt-[10px]" />
              </div>
              <p className=" ml-[20px]">
                50 000 довольных <br /> клиентов по всей страна
              </p>
            </div>
            <div className=" flex items-center">
              <div className=" bg-pink-200  w-[72px] h-[72px] rounded-full text-center">
                <img
                  src={selfAdhesive}
                  alt=""
                  className="ml-[13px] mt-[13px]"
                />
              </div>
              <p className=" ml-[20px]">
                99% заказов приходит <br />в назначенное время
              </p>
            </div>
            <div className="flex items-center">
              <div className=" bg-light-blue-200 w-[72px] h-[72px] rounded-full">
                <img src={market} alt="" className="mt-[10px] ml-[11px]" />
              </div>
              <p className="ml-[20px]">
                5 лет на рынке <br />
                инструмента и техники
              </p>
            </div>
          </div>
          <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto xl:flex-wrap gap-4 mt-[86px]">
            <div className="flex items-center ml-[310px]">
              <div className=" bg-orange-200 w-[72px] h-[72px] rounded-full">
                <img src={box} alt="" className=" mt-[10px] ml-[11px]" />
              </div>
              <p className="ml-[20px]">
                Боле 5 000 позиций <br />
                товаров на складах
              </p>
            </div>
            <div className="flex items-center mr-[200px]">
              <div className=" bg-green-100 w-[72px] h-[72px] rounded-full">
                <img src={car} alt="" className=" mt-[10px] ml-[11px]" />
              </div>
              <p className="ml-[20px]">
                Бесплатная доставка по городу <br />
                Ташкент (при заказе от 3 млн.)
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Advantages;
