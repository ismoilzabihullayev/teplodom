import React from "react";
import Phone1 from "../../assets/phone/clarity_mobile-phone-line.svg";
const Phone = () => {
  return (
    <div>
      <div className=" container ">
        <div className=" text-3xl mt-[20px]">
          <h1>Контакты</h1>
        </div>
        <div className="flex mt-[30px] mb-[50px]">
          <div className="flex ml-[-90px]">
            <div className="mt-[10px] w-auto ">
              <img src={Phone1} className="ml-[280px]" alt="" />
              <span className=" ml-[285px]">Телефон</span>
              <br />
              <span className=" ml-[246px]">+998 (33) 200-72-77</span>
            </div>
          </div>
          <div className="flex">
            <div className="mt-[10px] w-auto ">
              <img src={Phone1} className="ml-[280px]" alt="" />
              <span className=" ml-[285px]">Телефон</span>
              <br />
              <span className=" ml-[246px]">+998 (33) 200-72-77</span>
            </div>
          </div>
          <div className="flex">
            <div className="mt-[10px] ">
              <img src={Phone1} className="ml-[280px]" alt="" />
              <span className=" ml-[285px]">Телефон</span>
              <br />
              <span className=" ml-[246px]">+998 (33) 200-72-77</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Phone;
