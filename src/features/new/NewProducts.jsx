import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getNewProducts } from "./newProductsSlice";
import ProductCard from "../../components/ProductCard";
import { Link } from "react-router-dom";

const NewProducts = () => {
  const { newProducts } = useSelector((state) => state.newProducts);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getNewProducts());
  }, []);
  return (
    <div className="mt-[76px]">
      <div className="flex items-center justify-between mb-8">
        <h1 className="lg:text-4xl">Новинки на сайте</h1>
        <Link to={"new"} className="text-sm text-[#0077B6] lg:text-lg">
          Смотреть все →
        </Link>
      </div>
      <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto gap-10 py-5 object-fill">
        {newProducts?.map((product, i) => {
          if (i >= 4) {
            return null;
          } else {
            return <ProductCard key={product.id} product={product} />;
          }
        })}
      </div>
      <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto gap-10 py-5 object-fill">
        {newProducts?.map((product, i) => {
          if (i < 6) {
            return null;
          } else {
            return <ProductCard key={product.id} product={product} />;
          }
        })}
      </div>
    </div>
  );
};

export default NewProducts;
