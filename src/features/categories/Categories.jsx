import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCategories } from "./categorySlice";
import { Link } from "react-router-dom";

const Categories = () => {
  const { categories } = useSelector((state) => state.categories);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCategories());
  }, []);
  return (
    <div className=" container ">
      <div className="mt-[76px]">
        <div className="flex items-center justify-between mb-8">
          <h1 className="lg:text-3xl">Категории</h1>
          <Link to={"categories"} className="text-sm text-[#0077B6] lg:text-lg">
            Все категории →
          </Link>
        </div>
        <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto xl:flex-wrap gap-4">
          {categories?.map((category, i) => {
            if (i > 5) {
              return null;
            } else {
              return (
                <div
                  key={category.id}
                  className="min-w-[140px] h-[170px] lg:w-[170px] lg:h-[200px] flex flex-col bg-white rounded-xl p-3 cursor-pointer object-fill	"
                >
                  <div className="flex flex-1 items-center justify-center">
                    <img src={category.image} className="max-w-[129px]" />
                  </div>
                  <p className="text-center line-clamp-2">{category.name}</p>
                </div>
              );
            }
          })}
        </div>
      </div>
    </div>
  );
};

export default Categories;
